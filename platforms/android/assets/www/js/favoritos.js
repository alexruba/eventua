var Fav = {
    
    tiposExplorados : [],
    idExplorados:[],
    
    rellenarFavoritos: function()
    {
        var firstTime = true;
        idExplorados = [];
        tiposExplorados = [];
        var eventosFav = JSON.parse(localStorage.getItem('listaFavoritos'));
        var tipos = JSON.parse(localStorage.getItem('listaTipos'));

        for(var j = 0; j < tipos.length; j++)
        {
            if(!Fav.esExplorado(tipos[j].tipo))
            {
                Fav.tiposExplorados.push(tipos[j].tipo);
                for(var i = 0; i < eventosFav.length; i++)
                {
                    if(localStorage.getItem("eventoTipo" + eventosFav[i].id) == tipos[j].tipo
                       && localStorage.getItem("eventoFav" + eventosFav[i].id) == "Y" && !Fav.idExplorado(eventosFav[i].id))
                    {
                        Fav.idExplorados.push(eventosFav[i].id);
                        
                        if(firstTime)
                        {
                            $("#listaFavoritos").append("<li data-role='list-divider'>" + tipos[j].tipo + "</li>");
                            firstTime = false;
                        }
                        
                        $("#listaFavoritos").append(
                            "<li id='" + eventosFav[i].id + "'><a href='details.html'><h2>" + localStorage.getItem("eventoNombre" + eventosFav[i].id) + "</h2><p><strong>" + localStorage.getItem("eventoLugar" + eventosFav[i].id) + "</strong></p><p>" + localStorage.getItem("eventoFecha" + eventosFav[i].id) + "</p></strong></p></a></li>"
                        );
                    }
                }
                firstTime = true;
            }
        }
    },
    
    esExplorado:function(tipo)
    {
        for(var i = 0; i < Fav.tiposExplorados.length; i++)
        {
            if(Fav.tiposExplorados[i] === tipo)
                return true;
        }
        
        return false;
    },
    
    idExplorado:function(id)
    {
        for(var i = 0; i < Fav.idExplorados.length; i++)
        {
            if(Fav.idExplorados[i] === id)
                return true;
        }
        
        return false;
    },
    
    
};

$( document ).ready(function()
{
                    
        $('listaFavoritos').remove();
        var ul = document.getElementById('listaFavoritos'); // Parent

        ul.addEventListener('click', function (e) {
            var target = e.target; // Clicked element
            while (target && target.parentNode !== ul) {
                target = target.parentNode; // If the clicked element isn't a direct child
                if(!target) { return; } // If element doesn't exist
            }
            if (target.tagName === 'LI'){
                localStorage.setItem("eventClicked", target.id);
            }
        });
        var eventosFav = JSON.parse(localStorage.getItem('listaFavoritos'));

        if(eventosFav != null)
            Fav.rellenarFavoritos();
});
