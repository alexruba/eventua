function Evento(evento)
{
    this.titulo =  evento.titulo;
    this.sigua = evento.sigua;
    this.lugar = evento.lugar;
    this.edificio = evento.edificio;
    this.fecha = evento.fecha;
    this.horario = evento.horario;
    this.materia = evento.materia;
    this.tipo = evento.tipo;
    this.coordenadas = evento.coordenadas;
    this.id = evento.id;
    
    return this;
}