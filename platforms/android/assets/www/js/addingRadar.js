// information about server communication. This sample webservice is provided by Wikitude and returns random dummy places near given location
var ServerInformation = {
    EDIFICIOS_TEST: "https://catalogo.datos.ua.es/datasets/Edificios%20de%20la%20Universidad%20de%20Alicante/Edificios%20de%20l a%20Universidad%20de%20Alicante.json",
    EDIFICIOS: "https://dev.datos.ua.es/uapi/pA94GaKZHxUmN9WTmpEE/datasets/11/data/",
    EDIFICIO_SIGUA: "http://www.sigua.ua.es/api/pub/estancia/",
    ESPACIOS: "https://catalogo.datos.ua.es/datasets/Datos%20Gestion%20de%20espacios%20UA/Datos%20Gestion%20de%20espacios%20UA.csv",
};

// implementation of AR-Experience (aka "World")
var World = {

    // you may request new data from server periodically, however: in this sample data is only requested once
    isRequestingData: false,
    
    eventosList: [],
    eventosFav: [],
    eventSelected: null,

    // true once data was fetched
    initiallyLoadedData: false,

    // different POI-Marker assets
    markerDrawable_idle: null,
    markerDrawable_selected: null,
    markerDrawable_directionIndicator: null,

    // list of AR.GeoObjects that are currently shown in the scene / World
    markerList: [],

    // The last selected marker
    currentMarker: null,

    locationUpdateCounter: 0,
    updatePlacemarkDistancesEveryXLocationUpdates: 10,
    
    maxRange: 1000,

    // called to inject new POI data
    loadPoisFromJsonData: function loadPoisFromJsonDataFn(poiData) {

        // show radar & set click-listener
        PoiRadar.show();
        $('#radarContainer').unbind('click');
        $("#radarContainer").click(PoiRadar.clickedRadar);

        // empty list of visible markers
        World.markerList = [];
        
        // start loading marker assets
        World.markerDrawable_idle = new AR.ImageResource("assets/marker_idle.png");
        World.markerDrawable_selected = new AR.ImageResource("assets/marker_selected.png");
        World.markerDrawable_directionIndicator = new AR.ImageResource("assets/indi.png");
        
        // loop through POI-information and create an AR.GeoObject (=Marker) per POI
        for (var currentPlaceNr = 0; currentPlaceNr < poiData.length; currentPlaceNr++)
        {
            var coordinates = poiData[currentPlaceNr].coordenadas.toString().split(",");
            var latitude = parseFloat(coordinates[1]);
            var longitude = parseFloat(coordinates[0]);

            var singlePoi = {
                    "id": poiData[currentPlaceNr].id,
                    "latitude": latitude,
                    "longitude": longitude,
                    "altitude": 100,
                    "title": poiData[currentPlaceNr].titulo,
                    "place": poiData[currentPlaceNr].lugar,
                    "date": poiData[currentPlaceNr].fecha,
                    "timetable": poiData[currentPlaceNr].horario,
                    "type": poiData[currentPlaceNr].tipo,
                    "subject": poiData[currentPlaceNr].materia
            };
            
            localStorage.setItem("eventoLat" + poiData[currentPlaceNr].id, latitude);
            localStorage.setItem("eventoLong" + poiData[currentPlaceNr].id, longitude);
            
            
            World.markerList.push(new Marker(singlePoi));
        }

        // updates distance information of all placemarks
        World.updateDistanceToUserValues();
        
        World.updateSearchList();
        
        World.updateRangeValues();
        
        // set distance slider to 100%
        $("#panel-distance-range").val(100);
        $("#panel-distance-range").slider("refresh");
        
    },

    
    updateSearchList: function ()
    {
        var items = [];
        
        $.each(World.markerList, function(i, item)
        {
               items.push('<li data-id=' + item.id + ' class="ui-screen-hidden"><a href="">' + item.name.charAt(0).toUpperCase() + item.name.slice(1).toLowerCase() + '</a></li>');
        });
        
        $('#listaEdificios').append( items.join('') );
        $('#listaEdificios').listview('refresh');
        
        $('#searchDiv ul').children('li').bind('touchstart mousedown', function(e) {
                $('#popupSearch').fadeOut();
                World.buscar($(this).attr('data-id'));
        });
        
        $('#searchButton').bind('touchstart mousedown', function(e) {
                $('#popupSearch').fadeIn();
                $('#listaEdificios').listview('refresh');
                $('input[data-type="search"]').val("");
                $('input[data-type="search"]').trigger("keyup");
        });
        
    },
    

    // sets/updates distances of all makers so they are available way faster than calling (time-consuming) distanceToUser() method all the time
    updateDistanceToUserValues: function updateDistanceToUserValuesFn()
    {
        for (var i = 0; i < World.markerList.length; i++) {
            World.markerList[i].distanceToUser = World.markerList[i].markerObject.locations[0].distanceToUser();
        }
    },

    // location updates, fired every time you call architectView.setLocation() in native environment
    locationChanged: function locationChangedFn(lat, lon, alt, acc)
    {
        // request data if not already present
        if (!World.initiallyLoadedData) {
            World.requestDataFromServer(lat, lon);
            World.initiallyLoadedData = true;
        } else if (World.locationUpdateCounter === 0) {
            // update placemark distance information frequently, you max also update distances only every 10m with some more effort
            World.updateDistanceToUserValues();
        }

        // helper used to update placemark information every now and then (e.g. every 10 location updates fired)
        World.locationUpdateCounter = (++World.locationUpdateCounter % World.updatePlacemarkDistancesEveryXLocationUpdates);
    },

	// fired when user pressed marker in cam
	onMarkerSelected: function onMarkerSelectedFn(marker) {
		World.currentMarker = marker;

		// update panel values
		$("#poi-detail-title").html(marker.poiData.title);
		$("#poi-detail-place").html(marker.poiData.place);
        $("#poi-detail-date").html(marker.poiData.date);
        $("#poi-detail-timetable").html(marker.poiData.timetable);
        $("#poi-detail-type").html(marker.poiData.type);
        $("#poi-detail-subject").html(marker.poiData.subject);

		var distanceToUserValue = (marker.distanceToUser > 999) ? ((marker.distanceToUser / 1000).toFixed(2) + " km") : (Math.round(marker.distanceToUser) + " m");

		$("#poi-detail-distance").html(distanceToUserValue);

		// show panel
		$("#panel-poidetail").panel("open", 123);
        
        if(localStorage.getItem("eventoFav" + World.currentMarker.id) == "Y")
            document.getElementById("image1").src="img/staron.png";
        else
            document.getElementById("image1").src="img/staroff.png";
		
		$( ".ui-panel-dismiss" ).unbind("mousedown");
        
		$("#panel-poidetail").on("panelbeforeclose", function(event, ui) {
			World.currentMarker.setDeselected(World.currentMarker);
		});
	},
    
    closeMap: function()
    {
        document.getElementById('searchButton').style.display = 'block';
        document.getElementById('refreshButton').style.display = 'block';
        document.getElementById('closeMapButton').style.display = 'none';
        document.getElementById('map').style.display = 'none';
    },

	// screen was clicked but no geo-object was hit
	onScreenClick: function onScreenClickFn() {
		// you may handle clicks on empty AR space too
	},

	// returns distance in meters of placemark with maxdistance * 1.1
	getMaxDistance: function getMaxDistanceFn() {

		// sort palces by distance so the first entry is the one with the maximum distance
		World.markerList.sort(World.sortByDistanceSortingDescending);

		// use distanceToUser to get max-distance
		var maxDistanceMeters = World.markerList[0].distanceToUser;

		// return maximum distance times some factor >1.0 so ther is some room left and small movements of user don't cause places far away to disappear
		return maxDistanceMeters * 1.1;
	},

    // udpates values show in "range panel"
    updateRangeValues: function updateRangeValuesFn() {
        
        // get current slider value (0..100);
        var slider_value = $("#panel-distance-range").val();
        
        // max range relative to the maximum distance of all visible places
        var maxRangeMeters = Math.round(World.maxRange * (slider_value / 100));
        
        // range in meters including metric m/km
        
        var maxRangeValue = (maxRangeMeters > 999) ? ((maxRangeMeters / 1000).toFixed(2) + " km") : (Math.round(maxRangeMeters) + " m");
        
        //var maxRangeValue = (maxRangeMeters > World.maxRange - 1) ? ((maxRangeMeters / World.maxRange).toFixed(2) + " km") : (Math.round(maxRangeMeters) + " m");
        
        // number of places within max-range
        var placesInRange = World.getNumberOfVisiblePlacesInRange(maxRangeMeters);
        
        // update UI labels accordingly
        $("#panel-distance-value").html(maxRangeValue);
        $("#panel-distance-places").html((placesInRange != 1) ? (placesInRange + " Edificios") : (placesInRange + " Edificio"));
        
        // update culling distance, so only places within given range are rendered
        AR.context.scene.cullingDistance = Math.max(maxRangeMeters, 1);
        
        // update radar's maxDistance so radius of radar is updated too
        PoiRadar.setMaxDistance(Math.max(maxRangeMeters, 1));
    },
        
        // returns number of places with same or lower distance than given range
    getNumberOfVisiblePlacesInRange: function getNumberOfVisiblePlacesInRangeFn(maxRangeMeters) {
        
        // sort markers by distance
        World.markerList.sort(World.sortByDistanceSorting);
        
        // loop through list and stop once a placemark is out of range ( -> very basic implementation )
        for (var i = 0; i < World.markerList.length; i++) {
            if (World.markerList[i].distanceToUser > maxRangeMeters) {
                return i;
            }   
        };
        
        // in case no placemark is out of range -> all are visible
        return World.markerList.length;
    },

    handlePanelMovements: function handlePanelMovementsFn() {

        $("#panel-distance").on("panelclose", function(event, ui) {
            $("#radarContainer").addClass("radarContainer_left");
            $("#radarContainer").removeClass("radarContainer_right");
            PoiRadar.updatePosition();
        });

        $("#panel-distance").on("panelopen", function(event, ui) {
            $("#radarContainer").removeClass("radarContainer_left");
            $("#radarContainer").addClass("radarContainer_right");
            PoiRadar.updatePosition();
        });
    },
    
    // display range slider
    showRange: function showRangeFn() {
        if (World.markerList.length > 0) {
            
            // update labels on every range movement
            $('#panel-distance-range').change(function() {
                                              World.updateRangeValues();
                                              });
            
            World.updateRangeValues();
            World.handlePanelMovements();
            
            // open panel
            $("#panel-distance").trigger("updatelayout");
            $("#panel-distance").panel("open", 1234);
        } else {
            
            // no places are visible, because the are not loaded yet
            World.updateStatusMessage('Todavía no se han encontrado edificios.', true);
        }
    },

	// request POI data
	requestDataFromServer: function requestDataFromServerFn(lat, lon) {

		// set helper var to avoid requesting places while loading
		World.isRequestingData = true;
        
        var eventos = World.getEventosValues();
        var espacios = World.getEspaciosValues();
        var url;
        var coord;
        var coordenadasJSON;
        var longitude;
        var latitude;
        var middle;
        
        for(var i = 0; i < eventos.length; i++)
        {
            coord = World.getCoordinates(eventos[i].SIGUA);
            coordenadasJSON = coord.features[0].geometry.coordinates[0][0][0].toString().split(",");
            
            for(var j = 0; j < espacios.length; j++)
            {
                if(espacios[j].ID_SIGUA === eventos[i].SIGUA)
                {
                    middle = Math.floor((eventos[i].Materia.length) / 2);
                    
                    evento = {
                        "id": eventos[i].Referencia,
                        "titulo": eventos[i].Titulo,
                        "sigua": eventos[i].SIGUA,
                        "lugar": espacios[j].AUL_DESID1,
                        "edificio": espacios[j].EDI_DESID1,
                        "fecha": eventos[i].Fechas,
                        "horario": eventos[i].Horario,
                        "materia": eventos[i].Materia.substr(0, middle),
                        "tipo": eventos[i].Tipo,
                        "coordenadas": coordenadasJSON
                    };
                    localStorage.setItem("eventoID" + evento.id, evento.id);
                    localStorage.setItem("eventoNombre" + evento.id, evento.titulo);
                    localStorage.setItem("eventoLugar" + evento.id, evento.lugar);
                    localStorage.setItem("eventoFecha" + evento.id, evento.fecha);
                    
                    if(evento.horario != null)
                        localStorage.setItem("eventoHorario" + evento.id, evento.horario);
                    else
                        localStorage.setItem("eventoHorario" + evento.id, "Hora no definida");
                    
                    localStorage.setItem("eventoTipo" + evento.id, evento.tipo);
                    localStorage.setItem("eventoMateria" + evento.id, evento.materia);
                    
                    World.eventosList.push(new Evento(evento));
                }
            }
        }
        
        World.loadPoisFromJsonData(World.eventosList);
	},
   
    
    getEspaciosValues: function()
    {
        var espaciosTmp = null;
        $.ajax({
               url: 'js/espacios.json',
               dataType: 'json',
               async: false,
               success: function(response) {
               espaciosTmp = response;
               },
               error: function(jqXHR, status, error){
               alert(jqXHR.status);
               World.isRequestingData = false;
               },
               complete:function(jqXHR, status){
               World.isRequestingData = false;
               }
               });
        return espaciosTmp;
    },
    
    getCoordinates: function(sigua) {
        var coordinatesTmp = null;

        $.ajax({
            url: ServerInformation.EDIFICIO_SIGUA + sigua,
            type: 'GET',
            dataType: 'json',
            async:false,
            success: function(response) {
                coordinatesTmp = response;
            },
            error: function(jqXHR, status, error){
                alert(jqXHR.status);
                alert(status);
                World.isRequestingData = false;
            },
            complete:function(response){
                World.isRequestingData = false;
            }
        });
        return coordinatesTmp;
    },
    
    getEventosValues: function() {
        var eventosTmp = null;
        $.ajax({
               url: 'js/eventos.js',
               dataType: 'json',
               async: false,
               success: function(response) {
               eventosTmp = response;
               },
               error: function(jqXHR, status, error){
               alert(jqXHR.status);
               World.isRequestingData = false;
               },
               complete:function(jqXHR, status){
               World.isRequestingData = false;
               }
               });
        return eventosTmp;
    },

    
	// helper to sort places by distance
	sortByDistanceSorting: function(a, b) {
		return a.distanceToUser - b.distanceToUser;
	},

	// helper to sort places by distance, descending
	sortByDistanceSortingDescending: function(a, b) {
		return b.distanceToUser - a.distanceToUser;
	},
    
    buscar: function(id)
    {
        var evento;
        
        for(var i = 0; i < World.markerList.length; i++)
        {
            if(World.markerList[i].id == id)
                evento = World.markerList[i];
        }
        
        if(evento.name !== "")
        {
            for(var i = 0; i < World.markerList.length; i++)
            {
                if(World.markerList[i].name.toLowerCase() != evento.name.toLowerCase())
                    World.markerList[i].markerObject.enabled = false;
                else
                    World.markerList[i].markerObject.enabled = true;
            }
        }
        else
        {
            for(var i = 0; i < World.markerList.length; i++)
                    World.markerList[i].markerObject.enabled = true;
        }
    },
    
    verTodo: function()
    {
        for(var i = 0; i < World.markerList.length; i++)
            World.markerList[i].markerObject.enabled = true;
    },
    
    createArray: function(length) {
        var arr = new Array(length || 0),
        i = length;
        
        if (arguments.length > 1) {
            var args = Array.prototype.slice.call(arguments, 1);
            while(i--) arr[length-1 - i] = createArray.apply(this, args);
        }
        
        return arr;
    },

    
    showMap: function() {
        
        document.getElementById('map').style.display = 'block';
        $('#map').css('position', 'absolute');
        $("#panel-poidetail").panel("close", 123);
        
        document.getElementById('refreshButton').style.display = 'none';
        document.getElementById('searchButton').style.display = 'none';
        document.getElementById('closeMapButton').style.display = 'block';
        
        var map = new GMaps({
                            div: '#map',
                            lat: World.currentMarker.poiData.latitude,
                            lng: World.currentMarker.poiData.longitude,
                            width: '100%',
                            height: '100%',
                            zoom: 18,
                            zoomControl: true,
                            zoomControlOpt: {
                                style: 'SMALL',
                                position: 'TOP_LEFT'
                            },
                            panControl: false
                            });
        map.addMarker({
                      lat: World.currentMarker.poiData.latitude,
                      lng: World.currentMarker.poiData.longitude,
                      title: World.currentMarker.poiData.title,
                      click: function(e) {
                        alert(World.currentMarker.poiData.title);
                      }
                      });
        
        GMaps.geolocate({
                        success: function(position) {
                            map.addMarker({
                                      lat: position.coords.latitude,
                                      lng: position.coords.longitude,
                                      });
                            map.drawRoute({
                                          origin: [World.currentMarker.poiData.latitude, World.currentMarker.poiData.longitude],
                                          destination: [position.coords.latitude, position.coords.longitude],
                                          strokeColor: '#FA5858',
                                          strokeOpacity: 0.6,
                                          strokeWeight: 6
                                          });
                        },
                        error: function(error) {
                        alert('La geolocalización ha fallado: ' + error.message);
                        },
                        not_supported: function() {
                        alert("Tu navegador no soporta geolocalización");
                        },
                        always: function() {
                        }
                        });
        
    },
    
    diffImage: function(img)
    {
        if(img.src.match(/staroff.png/))
        {
            img.src = "img/staron.png";
            localStorage.setItem("eventoFav" + World.currentMarker.id, "Y");
            var eventosFavTemp = JSON.parse(localStorage.getItem('listaFavoritos')) || [];
            eventosFavTemp.push({id:World.currentMarker.id});
            localStorage.setItem('listaFavoritos', JSON.stringify(eventosFavTemp));
            var eventosTipoTmp = JSON.parse(localStorage.getItem('listaTipos')) || [];
            eventosTipoTmp.push({tipo:World.currentMarker.type});
            localStorage.setItem('listaTipos', JSON.stringify(eventosTipoTmp));
            
        }
        else
        {
            img.src = "img/staroff.png";
            localStorage.setItem("eventoFav" + World.currentMarker.id, "N");
        }
    },
    

};


/* forward locationChanges to custom function */
AR.context.onLocationChanged = World.locationChanged;

/* forward clicks in empty area to World */
AR.context.onScreenClick = World.onScreenClick;