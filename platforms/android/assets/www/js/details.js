var Detail = {
    
    eventClicked: "",
    
    getEventDetails: function()
    {
        Detail.eventClicked = localStorage.getItem('eventClicked');
        
        // update panel values
        $("#poi-detail-title").html(localStorage.getItem('eventoNombre' + Detail.eventClicked));
        $("#poi-detail-place").html(localStorage.getItem('eventoLugar' + Detail.eventClicked));
        $("#poi-detail-date").html(localStorage.getItem('eventoFecha' + Detail.eventClicked));
        $("#poi-detail-timetable").html(localStorage.getItem('eventoHorario' + Detail.eventClicked));
        $("#poi-detail-type").html(localStorage.getItem('eventoTipo' + Detail.eventClicked));
        $("#poi-detail-subject").html(localStorage.getItem('eventoMateria' + Detail.eventClicked));
    },
    
    removeFromFavoritos: function(img)
    {
        localStorage.setItem("eventoFav" + Detail.eventClicked, "N");
        $.mobile.changePage("favoritos.html");
    },
    
    showMap: function() {
    
        document.getElementById('map').style.display = 'block';
        $('#map').css('position', 'absolute');
        
        document.getElementById('favoritosButton').style.display = 'none';
        document.getElementById('content').style.display = 'none';
        document.getElementById('buttonsDiv').style.display = 'none';
        document.getElementById('closeMapButton').style.display = 'block';
        
        var map = new GMaps({
                            div: '#map',
                            lat: localStorage.getItem('eventoLat' + Detail.eventClicked),
                            lng: localStorage.getItem('eventoLong' + Detail.eventClicked),
                            width: '100%',
                            height: '100%',
                            zoom: 18,
                            zoomControl: true,
                            zoomControlOpt: {
                            style: 'SMALL',
                            position: 'TOP_LEFT'
                            },
                            panControl: false
                            });
        map.addMarker({
                      lat: localStorage.getItem('eventoLat' + Detail.eventClicked),
                      lng: localStorage.getItem('eventoLong' + Detail.eventClicked),
                      title: localStorage.getItem('eventoNombre' + Detail.eventClicked),
                      click: function(e) {
                      alert(localStorage.getItem('eventoNombre' + Detail.eventClicked));
                      }
                      });
        
        GMaps.geolocate({
                        success: function(position) {
                        map.addMarker({
                                      lat: position.coords.latitude,
                                      lng: position.coords.longitude,
                                      });
                        map.drawRoute({
                                      origin: [localStorage.getItem('eventoLat' + Detail.eventClicked), localStorage.getItem('eventoLong' + Detail.eventClicked)],
                                      destination: [position.coords.latitude, position.coords.longitude],
                                      strokeColor: '#FA5858',
                                      strokeOpacity: 0.6,
                                      strokeWeight: 6
                                      });
                        },
                        error: function(error) {
                        alert('La geolocalización ha fallado: ' + error.message);
                        },
                        not_supported: function() {
                        alert("Tu navegador no soporta geolocalización");
                        },
                        always: function() {
                        }
            });
    
    },
    
    closeMap: function()
    {
        document.getElementById('favoritosButton').style.display = 'block';
        document.getElementById('content').style.display = 'block';
        document.getElementById('buttonsDiv').style.display = 'block';
        document.getElementById('closeMapButton').style.display = 'none';
        document.getElementById('map').style.display = 'none';
    },

};

$( document ).ready(function() {
                    
    Detail.getEventDetails();
});
