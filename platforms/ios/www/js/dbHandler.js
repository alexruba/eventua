var db;

dbHandler = {
    initialize: function() {
        db = window.openDatabase('appdb', '1.0', 'AppDB', 5000000);
        db.transaction(dbHandler.createDB, dbHandler.errorCB, dbHandler.successCB);
    },
    createDB: function(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS eventos (eventoID integer unique, name text, place text, date text, time text, type text)');
        tx.executeSql('CREATE TABLE IF NOT EXISTS favoritos(favID unique, eventoID integer, FOREIGN KEY (eventoID) REFERENCES eventos(eventoID)');
    },
    errorCB: function(tx, err) {

    },
    successCB: function(msg) {

    },
    
    getDB:function(){return db;}
};